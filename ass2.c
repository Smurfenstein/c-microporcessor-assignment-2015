/*
* CAB202, Semester 2, 2015
* Assignment 2: Falling faces
*
* Author: Heath Mayocchi
* Student# n9378201
* Date: 18/10/2015
*/

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include "graphics.h"
#include "cpu_speed.h"
#include "sprite.h"
#include "usb_serial.h"

#define SCREEN_WIDTH 84
#define SCREEN_HEIGHT 48
#define SCREEN_TOP 8
#define FACE_SIZE 11
#define FACE_BUFFER 5
#define HERO_WIDTH 5
#define HERO_HEIGHT 7
# define SLOW 130
# define MEDIUM 100
# define FAST 70

// Global variables
uint8_t level = 1;
uint8_t score = 0;
uint8_t lives = 3;
uint8_t mad_count = 0;
uint8_t B1_pushed = 0;
uint8_t B0_pushed = 0;
Sprite angry;
Sprite happy;
Sprite mad;
Sprite hero;
uint8_t heroX = (SCREEN_WIDTH/2) - (HERO_WIDTH/2);
unsigned char hero_move_buff;
uint8_t old_angry_x;
uint8_t old_happy_x;
uint8_t old_mad_x;
uint8_t usb_int = 0;
uint8_t speed;

// Bitmaps
#define BYTES_PER_FACE 22
#define HERO_BYTES 7
unsigned char bm_angry[BYTES_PER_FACE] = {
    0b00011111, 0b00000000,
    0b00100000, 0b10000000,
    0b01000000, 0b01000000,
    0b10010001, 0b00100000,
    0b10001010, 0b00100000,
    0b10000000, 0b00100000,
    0b10001110, 0b00100000,
    0b10010001, 0b00100000,
    0b01000000, 0b01000000,
    0b00100000, 0b10000000,
    0b00011111, 0b00000000
};
unsigned char bm_happy[BYTES_PER_FACE] = {
    0b00011111, 0b00000000,
    0b00100000, 0b10000000,
    0b01000000, 0b01000000,
    0b10011011, 0b00100000,
    0b10011011, 0b00100000,
    0b10000000, 0b00100000,
    0b10100000, 0b10100000,
    0b10010001, 0b00100000,
    0b01001110, 0b01000000,
    0b00100000, 0b10000000,
    0b00011111, 0b00000000
};
unsigned char bm_mad[BYTES_PER_FACE] = {
    0b00011111, 0b00000000,
    0b00100000, 0b10000000,
    0b01000011, 0b01000000,
    0b10000011, 0b00100000,
    0b10011000, 0b00100000,
    0b10011000, 0b00100000,
    0b10000000, 0b00100000,
    0b10011111, 0b00100000,
    0b01000110, 0b01000000,
    0b00100000, 0b10000000,
    0b00011111, 0b00000000
};
unsigned char bm_hero[HERO_BYTES] = {
    0b10001000,
    0b10001000,
    0b10001000, 
    0b11111000, 
    0b10001000,
    0b10001000,
    0b10001000,
};

// Function declarations
int rand(void);
void srand ( unsigned int );
void init_hardware(void);
void adc_init(void);
void check_B0_press_debounced(void);
void check_B1_press_debounced(void);
void start_up(void);
void starting_level(void);
void level_pointer(void);
void setup_game(void);
void play_game(void);
void reset(void);
void reset_var(void);
void background(void);
void draw_sprites(void);
void move_hero(void);
void move_faces(void);
void hero_collision_detection(void);
void vertical_bounce(uint8_t);
void horizontal_bounce(uint8_t);
void level_three_respawn(void);
void face_collision_detection(void);
void initial_face_direction(uint8_t);
uint8_t random_x(uint8_t);
uint8_t random_y(void);
uint16_t adc_read(uint8_t);

/*****************************************************************************
Notes:

* Level 1 & 2 complete

* hero buffer for level 3 respawn
* bouncing is not always angle of incidence = reflection, fix colision detection
  
*****************************************************************************/

int main() {

	init_hardware();
	start_up();
	clear_screen();
	background();
	draw_sprites();
	show_screen();
	play_game();
    return 0;
}

void play_game(){

	while (lives > 0 && score < 20){
		if (level == 3){
			level_three_respawn();
			hero_move_buff = usb_serial_getchar();
		}
		move_hero();
		move_faces();
		hero_collision_detection();
		_delay_ms(1);
		clear_screen();
		background();
		draw_sprites();
		show_screen();
	}
	reset();
}
	
void face_collision_detection(void){
	
	// angry - happy
	if (angry.x + FACE_SIZE >= happy.x && angry.x + FACE_SIZE < happy.x + FACE_SIZE && angry.y > happy.y - FACE_SIZE && angry.y <= happy.y + FACE_SIZE){
		vertical_bounce(1);
		vertical_bounce(2);
	}
	if (angry.x > happy.x && angry.x <= happy.x + FACE_SIZE && angry.y > happy.y - FACE_SIZE && angry.y <= happy.y + FACE_SIZE){
		vertical_bounce(1);
		vertical_bounce(2);
	}
	if (angry.x > happy.x - FACE_SIZE && angry.x <= happy.x + FACE_SIZE && angry.y + FACE_SIZE >= happy.y && angry.y + FACE_SIZE < happy.y + FACE_SIZE){
		horizontal_bounce(1);
		horizontal_bounce(2);
	}
	if (angry.x > happy.x - FACE_SIZE && angry.x <= happy.x + FACE_SIZE && angry.y > happy.y && angry.y <= happy.y + FACE_SIZE){
		horizontal_bounce(1);
		horizontal_bounce(2);
	}
	// angry - mad
	if (angry.x + FACE_SIZE >= mad.x && angry.x + FACE_SIZE < mad.x + FACE_SIZE && angry.y > mad.y - FACE_SIZE && angry.y <= mad.y + FACE_SIZE){
		vertical_bounce(1);
		vertical_bounce(3);
	}
	if (angry.x > mad.x && angry.x <= mad.x + FACE_SIZE && angry.y > mad.y - FACE_SIZE && angry.y <= mad.y + FACE_SIZE){
		vertical_bounce(1);
		vertical_bounce(3);
	}
	if (angry.x > mad.x - FACE_SIZE && angry.x <= mad.x + FACE_SIZE && angry.y + FACE_SIZE >= mad.y && angry.y + FACE_SIZE < mad.y + FACE_SIZE){
		horizontal_bounce(1);
		horizontal_bounce(3);
	}
	if (angry.x > mad.x - FACE_SIZE && angry.x <= mad.x + FACE_SIZE && angry.y > mad.y && angry.y <= mad.y + FACE_SIZE){
		horizontal_bounce(1);
		horizontal_bounce(3);
	}
	// happy - mad
	if (happy.x + FACE_SIZE >= mad.x && happy.x + FACE_SIZE < mad.x + FACE_SIZE && happy.y > mad.y - FACE_SIZE && happy.y <= mad.y + FACE_SIZE){
		vertical_bounce(2);
		vertical_bounce(3);
	}
	if (happy.x > mad.x && happy.x <= mad.x + FACE_SIZE && happy.y > mad.y - FACE_SIZE && happy.y <= mad.y + FACE_SIZE){
		vertical_bounce(2);
		vertical_bounce(3);
	}
	if (happy.x > mad.x - FACE_SIZE && happy.x <= mad.x + FACE_SIZE && happy.y + FACE_SIZE >= mad.y && happy.y + FACE_SIZE < mad.y + FACE_SIZE){
		horizontal_bounce(2);
		horizontal_bounce(3);
	}
	if (happy.x > mad.x - FACE_SIZE && happy.x <= mad.x + FACE_SIZE && happy.y > mad.y && happy.y <= mad.y + FACE_SIZE){
		horizontal_bounce(2);
		horizontal_bounce(3);
	}
}

void initial_face_direction(uint8_t this_face){

	if (this_face == 1){
		uint8_t angry_dy = rand()%100;
		if (angry_dy < 12){			
			angry.dy = -1;
			angry.dx = 0;
		}
		else if (angry_dy < 25){
			angry.dy = -1;
			angry.dx = -1;
		}
		else if (angry_dy < 37){
			angry.dy = 1;
			angry.dx = 0;
		}
		else if (angry_dy < 50){
			angry.dy = -1;
			angry.dx = 1;
		}
		else if (angry_dy < 62){
			angry.dy = 0;
			angry.dx = -1;
		}
		else if (angry_dy < 75){
			angry.dy = 1;
			angry.dx = -1;
		}
		else if (angry_dy < 87){
			angry.dy = 0;
			angry.dx = 1;
		}
		else{
			angry.dy = 1;
			angry.dx = 1;
		}
	}
	if (this_face == 2){
		uint8_t happy_dy = rand()%100;
		if (happy_dy < 12){			
			happy.dy = -1;
			happy.dx = 0;
		}
		else if (happy_dy < 25){
			happy.dy = -1;
			happy.dx = -1;
		}
		else if (happy_dy < 37){
			happy.dy = 1;
			happy.dx = 0;
		}
		else if (happy_dy < 50){
			happy.dy = -1;
			happy.dx = 1;
		}
		else if (happy_dy < 62){
			happy.dy = 0;
			happy.dx = -1;
		}
		else if (happy_dy < 75){
			happy.dy = 1;
			happy.dx = -1;
		}
		else if (happy_dy < 87){
			happy.dy = 0;
			happy.dx = 1;
		}
		else{
			happy.dy = 1;
			happy.dx = 1;
		}
	}
	if (this_face == 3){
		uint8_t mad_dy = rand()%100;
		if (mad_dy < 12){			
			mad.dy = -1;
			mad.dx = 0;
		}
		else if (mad_dy < 25){
			mad.dy = -1;
			mad.dx = -1;
		}
		else if (mad_dy < 37){
			mad.dy = 1;
			mad.dx = 0;
		}
		else if (mad_dy < 50){
			mad.dy = -1;
			mad.dx = 1;
		}
		else if (mad_dy < 62){
			mad.dy = 0;
			mad.dx = -1;
		}
		else if (mad_dy < 75){
			mad.dy = 1;
			mad.dx = -1;
		}
		else if (mad_dy < 87){
			mad.dy = 0;
			mad.dx = 1;
		}
		else{
			mad.dy = 1;
			mad.dx = 1;
		}
	}
}

void level_three_respawn(void){
	
	if (angry.is_visible == 0){
		angry.x = random_x(1);
		angry.y = random_y();
		initial_face_direction(1);
	}
	else if (happy.is_visible == 0){
		happy.x = random_x(2);
		happy.y = random_y();
		initial_face_direction(2);
	}
	else if (mad.is_visible == 0){
		mad.x = random_x(3);
		mad.y = random_y();
		initial_face_direction(3);
	}
}

void vertical_bounce(uint8_t this_face){

	if (this_face == 1){
		angry.dx = -angry.dx;
	}
	else if (this_face == 2){
		happy.dx = -happy.dx;
	}
	else if (this_face == 3){
		mad.dx = -mad.dx;
	}
}

void horizontal_bounce(uint8_t this_face){

	if (this_face == 1){
		angry.dy = -angry.dy;
	}
	else if (this_face == 2){
		happy.dy = -happy.dy;
	}
	else if (this_face == 3){
		mad.dy = -mad.dy;
	}
}

void reset(void){
	
	if (lives == 0){
		draw_string(18, 10, "You Lose!");
		draw_string(15, 20, "<-- Reset");
		show_screen();
		while(1){
			check_B0_press_debounced();
			if (B0_pushed == 1){
				reset_var();
				init_hardware();
				start_up();
				clear_screen();
				background();
				draw_sprites();
				show_screen();
				play_game();
			}
		}
	}
	if (score == 20){
		draw_string(18, 10, "You Win!");
		draw_string(15, 20, "<-- Reset");
		show_screen();
		while(1){
			check_B0_press_debounced();
			if (B0_pushed == 1){
				reset_var();
				init_hardware();
				start_up();
				clear_screen();
				background();
				draw_sprites();
				show_screen();
				play_game();
			}
		}
	}
}

void reset_var(void){
	
	level = 1;
	score = 0;
	lives = 3;
	mad_count = 0;
	speed = SLOW;
	B0_pushed = 0;
	angry.x = random_x(1);
	angry.y = SCREEN_TOP;
	angry.is_visible = 1;				
	happy.x = random_x(2);
	happy.y = SCREEN_TOP;
	happy.is_visible = 1;
	mad.x = random_x(3);
	mad.y = SCREEN_TOP;
	mad.is_visible = 1;
	hero.x = heroX;
}

void hero_collision_detection(void){
	
	if (angry.is_visible == 1){
		if (hero.x <= angry.x + FACE_SIZE && hero.x >= angry.x && hero.y <= angry.y + FACE_SIZE-1 && hero.y >= angry.y){// Top left	
				lives --;
				angry.is_visible = 0;
		} 
		else if (hero.x + HERO_WIDTH <= angry.x + FACE_SIZE && hero.x + HERO_WIDTH >= angry.x && hero.y <= angry.y + FACE_SIZE-1 && hero.y >= angry.y){// Top right	
				lives --;
				angry.is_visible = 0;
		}
		else if (hero.x <= angry.x + FACE_SIZE && hero.x >= angry.x && hero.y + HERO_HEIGHT <= angry.y + FACE_SIZE && hero.y + HERO_HEIGHT >= angry.y){	// Bottom right
				lives --;
				angry.is_visible = 0;
		}
		else if (hero.x + HERO_WIDTH <= angry.x + FACE_SIZE && hero.x + HERO_WIDTH >= angry.x && hero.y + HERO_HEIGHT <= angry.y + FACE_SIZE && hero.y + HERO_HEIGHT >= angry.y){// Bottom left
				lives --;
				angry.is_visible = 0;
		} 
	}
	if (happy.is_visible == 1){
		if (hero.x <= happy.x + FACE_SIZE && hero.x >= happy.x && hero.y <= happy.y + FACE_SIZE-1 && hero.y >= happy.y){	
				score += 2;
				happy.is_visible = 0;
		} 
		else if (hero.x + HERO_WIDTH <= happy.x + FACE_SIZE && hero.x + HERO_WIDTH >= happy.x && hero.y <= happy.y + FACE_SIZE-1 && hero.y >= happy.y){		
				score += 2;
				happy.is_visible = 0;
		}
		else if (hero.x <= happy.x + FACE_SIZE && hero.x >= happy.x && hero.y + HERO_HEIGHT <= happy.y + FACE_SIZE && hero.y + HERO_HEIGHT >= happy.y){
				score += 2;
				happy.is_visible = 0;
		}
		else if (hero.x + HERO_WIDTH <= happy.x + FACE_SIZE && hero.x + HERO_WIDTH >= happy.x && hero.y + HERO_HEIGHT <= happy.y + FACE_SIZE && hero.y + HERO_HEIGHT >= happy.y){	
				score += 2;
				happy.is_visible = 0;
		} 
	}
	if (mad.is_visible == 1){
		if (hero.x <= mad.x + FACE_SIZE && hero.x >= mad.x && hero.y <= mad.y + FACE_SIZE-1 && hero.y >= mad.y){
				mad_count ++;
				mad.is_visible = 0;
		} 
		else if (hero.x + HERO_WIDTH <= mad.x + FACE_SIZE && hero.x + HERO_WIDTH >= mad.x && hero.y <= mad.y + FACE_SIZE-1 && hero.y >= mad.y){
				mad_count ++;
				mad.is_visible = 0;
		}
		else if (hero.x <= mad.x + FACE_SIZE && hero.x >= mad.x && hero.y + HERO_HEIGHT <= mad.y + FACE_SIZE && hero.y + HERO_HEIGHT >= mad.y){
				mad_count ++;
				mad.is_visible = 0;
		}
		else if (hero.x + HERO_WIDTH <= mad.x + FACE_SIZE && hero.x + HERO_WIDTH >= mad.x && hero.y + HERO_HEIGHT <= mad.y + FACE_SIZE && hero.y + HERO_HEIGHT >= mad.y){
				mad_count ++;
				mad.is_visible = 0;
		}
	}
}

uint8_t random_y(void){
	
	uint8_t max_y = SCREEN_HEIGHT - FACE_SIZE - 8;
	uint8_t rand_y = rand() % max_y;
	uint8_t proposed_y = rand_y + 8;
	return proposed_y;	
}

uint8_t random_x(uint8_t this_face){
	
	uint8_t max_x = SCREEN_WIDTH - FACE_SIZE;
	uint8_t proposed_x = rand() % max_x;
	old_angry_x = angry.x;
	old_happy_x = happy.x;
	old_mad_x= mad.x ;	
	if (this_face == 1){
		if (level != 3){
			if (proposed_x >= (old_angry_x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (old_angry_x + FACE_BUFFER + FACE_SIZE)){
			proposed_x = random_x(1);
			}
		}
		if (proposed_x >= (happy.x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (happy.x + FACE_BUFFER + FACE_SIZE)){
		proposed_x = random_x(1);
		}
		if (proposed_x >= (mad.x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (mad.x + FACE_BUFFER + FACE_SIZE)){
		proposed_x = random_x(1);
		}
		angry.is_visible = 1;
	}	
	else if (this_face == 2){
		if (level != 3){
			if (proposed_x >= (old_happy_x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (old_happy_x + FACE_BUFFER + FACE_SIZE)){
			proposed_x = random_x(2);
			}
		}
		if (proposed_x >= (angry.x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (angry.x + FACE_BUFFER + FACE_SIZE)){
		proposed_x = random_x(2);
		}	
		if (proposed_x >= (mad.x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (mad.x + FACE_BUFFER + FACE_SIZE)){
			proposed_x = random_x(2);
		}
		happy.is_visible = 1;
	}
	else if (this_face == 3){	
		if (level != 3){
					if (proposed_x >= (old_mad_x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (old_mad_x + FACE_BUFFER + FACE_SIZE)){
				proposed_x = random_x(3);
			}		
		}
		if (proposed_x >= (angry.x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (angry.x + FACE_BUFFER + FACE_SIZE)){
			proposed_x = random_x(3);
		}
		if (proposed_x >= (happy.x - FACE_BUFFER - FACE_SIZE) && proposed_x <= (happy.x + FACE_BUFFER + FACE_SIZE)){
			proposed_x = random_x(3);
		}
		mad.is_visible = 1;
	}	
	return proposed_x;
}

void move_faces(void){
	
	// Timer 0 values to control face movement speed
	speed = SLOW;	
	if (mad_count == 3){
		mad_count = 0;
	}	
	if ( mad_count == 0){		
		speed = SLOW;	
	}
	else if ( mad_count == 1){		
		speed = MEDIUM;	
	}
	else if ( mad_count == 2){		
		speed = FAST;	
	}	
	if (TCNT0 >= speed){		
		TCNT0 = 0;
		if (level == 1 || level == 2){
			angry.dy = 1;
			happy.dy = 1;
			mad.dy = 1;
			if (angry.y >= SCREEN_HEIGHT - FACE_SIZE){
				angry.x = random_x(1);
				angry.y = SCREEN_TOP;
			}
			else{		
				angry.y = angry.y + angry.dy;	
			}
			if (happy.y >= SCREEN_HEIGHT - FACE_SIZE){
				happy.x = random_x(2);
				happy.y = SCREEN_TOP;
			}
			else{		
				happy.y = happy.y + happy.dy;	
			}
			if (mad.y >= SCREEN_HEIGHT - FACE_SIZE){
				mad.x = random_x(3);
				mad.y = SCREEN_TOP;
			}
			else{
				mad.y = mad.y + mad.dy;
			}
		}
		if (level == 3){
			if (angry.y >= SCREEN_HEIGHT - FACE_SIZE){
				angry.y = SCREEN_HEIGHT - FACE_SIZE;
				horizontal_bounce( 1);
				angry.y = angry.y + angry.dy;
			}
			else if (angry.y <= SCREEN_TOP){
				angry.y = SCREEN_TOP;
				horizontal_bounce( 1);
				angry.y = angry.y + angry.dy;
			}
			else{
				angry.y = angry.y + angry.dy;
			}
			if (angry.x <= 0){
				angry.x = 0;
				vertical_bounce( 1);
				angry.x = angry.x + angry.dx;
			}
			else if (angry.x  >= SCREEN_WIDTH - FACE_SIZE){
				angry.x = SCREEN_WIDTH - FACE_SIZE;
				vertical_bounce( 1);
				angry.x = angry.x + angry.dx;
			}
			else{
				angry.x = angry.x + angry.dx;
			}
			if (happy.y >= SCREEN_HEIGHT - FACE_SIZE){	
				happy.y = SCREEN_HEIGHT - FACE_SIZE;
				horizontal_bounce( 2);
				happy.y = happy.y + happy.dy;
			}
			else if (happy.y <= SCREEN_TOP){
				happy.y = SCREEN_TOP;
				horizontal_bounce( 2);
				happy.y = happy.y + happy.dy;
			}
			else{				
				happy.y = happy.y + happy.dy;
			}
			if (happy.x <= 0){
				happy.x = 0;
				vertical_bounce( 2);
				happy.x = happy.x + happy.dx;
			}
			else if (happy.x  >= SCREEN_WIDTH - FACE_SIZE){
				happy.x = SCREEN_WIDTH - FACE_SIZE;
				vertical_bounce( 2);
				happy.x = happy.x + happy.dx;
			}
			else {
				happy.x = happy.x + happy.dx;
			}
			if (mad.y >= SCREEN_HEIGHT - FACE_SIZE){	
				mad.y = SCREEN_HEIGHT - FACE_SIZE;
				horizontal_bounce( 3);
				mad.y = mad.y + mad.dy;
			}
			else if (mad.y <= SCREEN_TOP){
				mad.y = SCREEN_TOP;
				horizontal_bounce( 3);
				mad.y = mad.y + mad.dy;
			}
			else{				
				mad.y = mad.y + mad.dy;
			}
			if (mad.x <= 0){
				mad.x = 0;
				vertical_bounce( 3);
				mad.x = mad.x + mad.dx;
			}
			else if (mad.x  >= SCREEN_WIDTH - FACE_SIZE){
				mad.x = SCREEN_WIDTH - FACE_SIZE ;
				vertical_bounce( 3);
				mad.x = mad.x + mad.dx;
			}
			else {
				mad.x = mad.x + mad.dx;
			}			
			face_collision_detection();
		}
	}
}

void move_hero(void){
	
	if (level == 1){
		if ((PINB >> PB0) & 1){
			if (hero.x <= 0){
				hero.x = 0;
			}
			else{
				if (TCNT1 >= 1250){
					hero.x = hero.x-3;
					TCNT1 = 0;
				} 
			}
		}
		else if ((PINB >> PB1) & 1){
			if (hero.x >= SCREEN_WIDTH - HERO_WIDTH){
				hero.x = SCREEN_WIDTH - HERO_WIDTH;
			}
			else{
				if (TCNT3 >= 1250){
					hero.x = hero.x+3;
					TCNT3 = 0;
				}
			}
		}
	}	
	else if (level == 2){		
		if (TCNT1 >= 1000){		
			uint16_t adc_result = adc_read(0); 
			 // Conversion of adc value to position values 	  
			 float max_adc = 1023.0;
			 long max_lcd_adc = (adc_result*(long)(LCD_X-5)) / max_adc;
			 hero.x = SCREEN_WIDTH - 5 - max_lcd_adc;
			TCNT1 = 0;
		}
	}
	else if (level == 3){
		if (hero_move_buff == 'w' || hero_move_buff == 'W'){
			usb_serial_flush_input();
			if (hero.y <= SCREEN_TOP){
				hero.y = SCREEN_TOP;
			}
			else{
				hero.y --;
			}
		}
		else if (hero_move_buff == 's' || hero_move_buff == 'S'){
			usb_serial_flush_input();
			if (hero.y >= SCREEN_HEIGHT - HERO_HEIGHT){
				hero.y = SCREEN_HEIGHT - HERO_HEIGHT;
			}
			else{
				hero.y ++;
			}
		}
		else if (hero_move_buff == 'a' || hero_move_buff == 'A'){
			usb_serial_flush_input();
			if (hero.x <= 0){
				hero.x = 0;
			}
			else{
				hero.x --;
			}
		}
		else if (hero_move_buff == 'd' || hero_move_buff == 'D'){
			usb_serial_flush_input();
			if (hero.x >= SCREEN_WIDTH - HERO_WIDTH){
				hero.x = SCREEN_WIDTH - HERO_WIDTH;
			}
			else{
				hero.x ++;
			}
		}
	}
}

void adc_init(void){
	
    // AREF = AVcc
	// ADMUX: ADC Multiplexer Selection Register
	// Changes the voltage reference, left adjust and the channel
	// REFS1 & REFS0 set to 0 & 1 = AVCC with external capacitor at AREF pin
    ADMUX = (1<<REFS0); 
    // ADC Enable and pre-scaler of 128
    // 8000000/128 = 62500
    ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
} 

uint16_t adc_read(uint8_t ch){
	
    // select the corresponding channel 0~7
    // ANDing with '7' will always keep the value
    // of 'ch' between 0 and 7
    ch &= 0b00000111;  // AND operation with 7
    ADMUX = (ADMUX & 0xF8)|ch;     // clears the bottom 3 bits before ORing 
    // start single conversion
    // write '1' to ADSC
    ADCSRA |= (1<<ADSC); 
    // wait for conversion to complete
    // ADSC becomes '0' again
    // till then, run loop continuously
    while(ADCSRA & (1<<ADSC)); 
    return (ADC);
}

void draw_sprites(void){	

	draw_sprite(&angry);
	draw_sprite(&happy);
	draw_sprite(&mad);
	draw_sprite(&hero);
}

void background(void){
	
	char lives_buff[8];
	char score_buff[8];
	char level_buff[8];
	draw_string(2, 0, "L: ");
    sprintf(lives_buff, "%d", lives);
	draw_string(14, 0, lives_buff);
	draw_string(25, 0, "S: ");
    sprintf(score_buff, "%d", score);
	draw_string(37, 0, score_buff);	
	draw_string(60, 0, "Lv: ");
    sprintf(level_buff, "%d", level);
	draw_string(76, 0, level_buff);	
	draw_line(0, 7, 83, 7);	
}

void setup_game(void){
	
	if(level == 3){
		init_sprite(&angry, random_x(1), random_y(), FACE_SIZE, FACE_SIZE, bm_angry);
		init_sprite(&happy, random_x(2), random_y(), FACE_SIZE, FACE_SIZE, bm_happy);	
		init_sprite(&mad, random_x(3), random_y(), FACE_SIZE, FACE_SIZE, bm_mad);
		initial_face_direction(1);
		initial_face_direction(2);		
		initial_face_direction(3);
	}
	else{
		init_sprite(&angry, random_x(1), SCREEN_TOP, FACE_SIZE, FACE_SIZE, bm_angry);
		init_sprite(&happy, random_x(2), SCREEN_TOP, FACE_SIZE, FACE_SIZE, bm_happy);
		init_sprite(&mad, random_x(3), SCREEN_TOP, FACE_SIZE, FACE_SIZE, bm_mad);
	}
	uint8_t heroY = SCREEN_HEIGHT - HERO_HEIGHT;
	init_sprite(&hero, heroX, heroY, HERO_WIDTH, HERO_HEIGHT, bm_hero);	
}

void start_up(void){

	draw_string(9, 2, "Falling Faces");
	draw_string(8, 12, "Heath Mayocchi");
	draw_string(0, 22, "Student# n9378201");
	draw_string(42, 40, "Menu -->");
	show_screen();
	while (B1_pushed == 0){
		check_B1_press_debounced();
	}
	clear_screen();
	B1_pushed = 0;	
	draw_string(0, 2, "Level:  1  2  3");
	draw_string(0, 30, "<-- Start game");
	draw_string(2, 40, "Choose level -->");
	draw_string(40, 12, "^");
	show_screen();
	starting_level();
}

void level_pointer(void){
	
	if (level == 1){
		draw_string(70, 12, " ");
		draw_string(40, 12, "^");
	}
	else if (level == 2){
		draw_string(40, 12, " ");
		draw_string(55, 12, "^");
	}
	else if (level == 3){
		draw_string(55, 12, " ");
		draw_string(70, 12, "^");
	}
	show_screen();
	starting_level();
}

void starting_level(void){

	unsigned int seed = 0;
    while (B0_pushed == 0 && B1_pushed == 0){
		check_B1_press_debounced();
		check_B0_press_debounced();
	}
	if (B0_pushed == 1){
		B0_pushed = 2;
		B1_pushed = 2;
		if (level == 3){
			if (usb_int == 0){
				usb_init();				
				// Wait until the 'player' is activated
				clear_screen();
				draw_string(0, 30, "Waiting for");
				draw_string(0, 40, "player...");
				show_screen();
				while(!(usb_serial_get_control() & USB_SERIAL_DTR));
				usb_int = 1;
			}
				
		}
		// Use the ADC ch 1 value for a random seed
		seed = adc_read(1);
		srand(seed);
		// Start game
		setup_game();
	}
	if (B1_pushed == 1){		
		// Choose level
		if (level == 3){			
			level = 1;
			B1_pushed = 0;
			level_pointer();
		}
		else{			
			level ++;
			B1_pushed = 0;
			level_pointer();
		}
	}
}

void init_hardware(void) {
	
	// Set CPU clock speed
    set_clock_speed(CPU_8MHz);
	// Initialising the LCD screen
	LCDInitialise(LCD_DEFAULT_CONTRAST);
	// Set buttons as inputs
	DDRB &= ~((1 << PB0) | (1 << PB1));	
    //Set to Normal Timer Mode for TCCR0B
    TCCR0B &= ~((1<<WGM02));
	// Set prescaler to 1024
    TCCR0B |= (1<<CS02)|(1<<CS00);
    TCCR0B &= ~((1<<CS01));
    //Set to Normal Timer Mode for TCCR1B
    TCCR1B &= ~((1<<WGM02));
	// Set prescaler to 1024
    TCCR1B |= (1<<CS02)|(1<<CS00);
    TCCR1B &= ~((1<<CS01));
    //Set to Normal Timer Mode for TCCR3B
    TCCR3B &= ~((1<<WGM02));
	// Set prescaler to 1024
    TCCR3B |= (1<<CS02)|(1<<CS00);
    TCCR3B &= ~((1<<CS01));	
	// Initialize ADC 
	adc_init();
}

void check_B0_press_debounced(void) {

    // Counter for number of equal states
    static uint8_t count = 0;
    // Keeps track of current (debounced) state
    static uint8_t button_state = 1;
    // Check if button is high or low for the moment
    uint8_t current_state = (~PINB & (1<<PINB0)) != 0;
    if (current_state != button_state) 
    {
        // Button state is about to be changed, increase counter
        count++;
        if (count >= 4) 
        {
            // The button have not bounced for two checks, change state
            button_state = current_state;
            // != for on release, == for on press
            if (current_state != 0) 
            {		
				B0_pushed = 1;
            }
            count = 0;
        }
    } else 
    {
        count = 0;
    }
}

void check_B1_press_debounced(void) {

    // Counter for number of equal states
    static uint8_t count = 0;
    // Keeps track of current (debounced) state
    static uint8_t button_state = 1;
    // Check if button is high or low for the moment
    uint8_t current_state = (~PINB & (1<<PINB1)) != 0;
    if (current_state != button_state) 
    {
        // Button state is about to be changed, increase counter
        count++;
        if (count >= 4) 
        {
            // The button have not bounced for two checks, change state
            button_state = current_state;
            // != for on release, == for on press
            if (current_state != 0) 
            {
				B1_pushed = 1;			
            }
            count = 0;
        }
    } else 
    {
        count = 0;
    }
}